## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. [Install](https://gohugo.io/getting-started/installing/) Hugo with `brew install hugo`
1. Preview your project (make sure you're within the directory):

   ```shell
   hugo server
   ```
1. Preview the site at http://localhost:1313/brewkeeper-landing-page/ (url & port will be specified in the terminal)

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

## Deploying

1. Commit, then push your code from `master`
2. After the pipeline has finished, view your changes here https://brewkeeper.gitlab.io/brewkeeper-landing-page
